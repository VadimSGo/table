#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <string>
#include "sqlite/sqlite3.h"
using namespace std;
const int E = 8;


class team {
public:
	std::string name;
	int matches, goalDifference, points;
	team(std::string name)//constructor
	{
		this->name = name;
		this->matches = 0;
		this->goalDifference = 0;
		this->points = 0;
	}
	~team() {};//destructor
};
void Fix(team T[], int E)
{
	std::cout << "\n        \Team     \tGames\tGoal difference\tPoints" << std::endl;
	for (int i = 0; i < E; i++)
	{
		printf("%17s %10d %10d %10d\n", T[i].name.c_str(), T[i].matches, T[i].goalDifference, T[i].points);
		//std::cout << T[i].name << '\t' << T[i].matches << '\t' << T[i].diffMass << '\t' << T[i].mass << std::endl;
	}
}

int CallBack(void *data, int argc, char **argv, char **ColName)
{
	for (int i = 0; i < argc; i++)
		std::cout << ColName[i] << " " << argv[i] << std::endl;
	return 0;
}

int main()
{
	setlocale(LC_ALL, "Russian");
	sqlite3* db = 0;
	char* error = 0;
	int answer = sqlite3_open("database.db", &db);
	if (answer != SQLITE_OK)
	{
		std::cout << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		return 0;
	}
	char SQL[100] = "SELECT * FROM DATA";
	answer = sqlite3_exec(db, SQL, &CallBack, 0, &error);
	if (answer != SQLITE_OK)
	{
		std::cout << error << std::endl;
		sqlite3_free(error);
	}
	int t1, t2;
	string name[8] = { "Arsenal", "LeisterCity", "ManchesterCity", "ManchesterUnited", "Liverpool", "TottenhamHotspur", "Everton", "Chelsea" };
	team teams[8] = { team(name[0]),
		team(name[1]),
		team(name[2]),
		team(name[3]),
		team(name[4]),
		team(name[5]),
		team(name[6]),
		team(name[7]) };
	int goal1, goal2;
	const int n = 2;
	int i = 0;
	cout << "����� ������������ � ����: �������1 �������2 ���� (����� ������)" << std::endl;
	while (i < 4 * n)
	{
		cin >> t1 >> t2 >> goal1 >> goal2;
		if (t1 == 0 || t2 == 0) {
			break;
		}
		if (t1 != t2) {
			if ((t1 < 0 && t1 > 8) || (t2 < 0 && t2 > 8)) {
				cout << "Uknown Team";
				return 0;
			}
			//set first team
			teams[t1 - 1].matches += 1;
			teams[t1 - 1].goalDifference += goal1 - goal2;
			if (goal1 > goal2) {
				teams[t1 - 1].points += 3;
			}
			else if (goal2 > goal1) {
				teams[t1 - 1].points += 0;
			}
			else {
				teams[t1 - 1].points += 1;
			}
			//set second team
			teams[t2 - 1].matches += 1;
			teams[t2 - 1].goalDifference += goal2 - goal1;
			if (goal2 > goal1) {
				teams[t2 - 1].points += 3;
			}
			else if (goal2 < goal1) {
				teams[t2 - 1].points += 0;
			}
			else {
				teams[t2 - 1].points += 1;
			}
			i++;
		}
		else {
			cout << "Not Correct";
			return 0;
		}
	}
	for (i = 0; i < 8; i++) {
		for (int j = 0; j < 7 - i; j++) {
			if (teams[j].points < teams[j + 1].points)
			{
				swap(teams[j], teams[j + 1]);
			}
			else if (teams[j].points == teams[j + 1].points) {//sort by gd
				if (teams[j].goalDifference < teams[j + 1].goalDifference) {
					swap(teams[j], teams[j + 1]);
				}
			}
		}
	}
	for (i = 0; i < 8; i++)
	{
		//  std::cout << "team "<<i<<" members : "<<teams.matches<<" "<<teams.points<<std::endl;
		std::string SQL1 = "INSERT  INTO EPL(Name, MATCHES,GOALDIFFERENCE, POINTS) VALUES (\'" + teams[i].name + "\'," + std::to_string(teams[i].matches) + ", " + std::to_string(teams[i].goalDifference) + ", " + std::to_string(teams[i].points) + ");";
		answer = sqlite3_exec(db, SQL1.c_str(), 0, 0, &error);
		if (answer != SQLITE_OK)
		{
			std::cout << error << std::endl;
			sqlite3_free(error);
		}
	}
	std::cout << "Enter results:\n";
	Fix(teams, E);
	return 0;
}